
function isPangram (sentence){

	/* Write you solution here */

	return true;
}



/* Tests -  Execute the following commands to install & execute Jasmine */
// sudo npm install karma --save-dev
// sudo npm install karma-jasmine karma-chrome-launcher karma-cli --save-dev
// ./node_modules/karma/bin/karma init 
//      file pattern -> ./pangram/pangram.js
// ./node_modules/karma/bin/karma start

describe("isPangram", function() {

	it("returns true when the given string contains every alphabet letter", function() {

		expect(isPangram("The quick brown fox jumps over the lazy dog")).toBe(true);
		expect(isPangram("The quick brown fox jumps over the lazy dog plus some more letters")).toBe(true);
		expect(isPangram("The quick brown fox jumps over the lazy dog, with weird cha@rs? O_o")).toBe(true);

	});

	it("returns false otherwise", function() {

		expect(isPangram("The quick brown fox")) .toBe(false);
		expect(isPangram("The quick brown fox.")).toBe(false);
		expect(isPangram("Test"))                .toBe(false);
		

	});

});